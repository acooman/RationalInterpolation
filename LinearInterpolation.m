classdef LinearInterpolation < Interpolation
    %LINEARINTERPOLATION 
    
    methods
        %% CONSTRUCTOR
        function obj = LinearInterpolation(varargin)
            obj = obj@Interpolation(varargin{:});
        end
    end
    
    %% Implement the abstract methods
    methods (Access=protected)
        function Z = evaluate_normalised(obj,x,index)
            % due to the normalisation, Z = ones(No,Ni).*x
            Z = repmat(x,[size(obj.Z,1),size(obj.Z,2),1]);
        end
        function obj = estimate_normalised(obj)
            % we have nothing to do here
        end
    end
end

