classdef Interpolation
    %INTERPOLATION 
    
    properties
        X         (1,1,:) double
        S         (1,1,:) double
        Z         (:,:,:) double
        OffsetS   (1,1,:) double
        DeltaS    (1,1,:) double
        OffsetZ   (:,:,:) double
        DeltaZ    (:,:,:) double
        Domain      (1,1) string
        Extrapolation (1,1) string
    end
    
    methods (Access=public)
        %% Constructor
        function obj=Interpolation(varargin)
            p=inputParser();
            p.StructExpand=true;
            p.addOptional('x',[],@Interpolation.checkParam);
            p.addOptional('Z',[],@Interpolation.checkData);
            p.addParameter("Domain","PLANE",@(x) any(Interpolation.checkDomain(x)));
            p.addParameter("Extrapolation","ZERO",@(x) any(Interpolation.checkExtrapolation(x)));
            p.parse(varargin{:});
            args = p.Results;
            obj.Domain = Interpolation.checkDomain(args.Domain);
            obj.Extrapolation = Interpolation.checkExtrapolation(args.Extrapolation);
            % if x and Z are provided, we can estimate the models. otherwise, we just have an interpolation object.
            if ~isempty(args.Z)
                % verify that the length of x and Z is equal
                if size(args.x,3)~=size(args.Z,3)
                    error('The third dimension of the parameter and the data should match');
                end
                % compute the offsets and deltas from the inputs
                obj = estimate(obj,args.x,args.Z);
            end
        end
        %% Estimate local models
        function obj = estimate(obj,x,Z)
            % do some checking on the inputs
            Interpolation.checkParamData(x,Z);
            % compute the offsets and deltas from the inputs
            obj = computeDeltasAndOffsets(obj,x,Z);
            % compute the local models
            obj = estimate_normalised(obj);
        end
        %% Evaluate local models
        function Z = evaluate(obj,x)
            x = x(:);
            F = length(x);
            % find the inband bins
            switch obj.Extrapolation
                case 'ZERO'
                    inband = (x<=(obj.X(end)+100*eps))&x>=(obj.X(1)-100*eps);
                case 'INTERPOLATION'
                    inband = logical(size(x));
            end
            % find out to which local model the data belongs
            c = obj.X(1,1,2:end-1);
            [~,index] = histc(x(inband),[-inf;c(:);inf]);
            % ensure that x is an 1x1xF matrix
            x = reshape(x(inband),1,1,[]);
            % compute the values in the complex plane
            s = domainParamtrisation(obj,x);
            % normalise the s-values
            s = (s-obj.OffsetS(1,1,index))./obj.DeltaS(1,1,index);
            % evaluate the normalised model
            Zt = evaluate_normalised(obj,s,index);
            % denormalise the model
            Zt = obj.DeltaZ(:,:,index).*Zt+obj.OffsetZ(:,:,index);
            % assign the result to the correct bins
            Z = zeros(size(Zt,1),size(Zt,2),F);
            Z(:,:,inband) = Zt;
        end
    end
    %% Abstract methods
    methods (Abstract,Access=protected)
        obj = estimate_normalised(obj);
        Z   = evaluate_normalised(obj,x,index);
    end
    %% protected methods
    methods (Access = protected)
        function obj=computeDeltasAndOffsets(obj,x,Z)
            [x,ord]=sort(x);
            obj.X = x;
            obj.S = domainParamtrisation(obj,x);
            obj.Z = double(Z(:,:,ord));
            obj.OffsetS = obj.S(:,:,1:end-1);
            obj.DeltaS  = obj.S(:,:,2:end)-obj.OffsetS;
            obj.OffsetZ = obj.Z(:,:,1:end-1);
            obj.DeltaZ  = obj.Z(:,:,2:end)-obj.OffsetZ;
        end
        function s=domainParamtrisation(obj,x)
            switch upper(obj.Domain)
                case 'PLANE'
                    s = 1i*2*pi*x;
                case 'DISC'
                    s = exp(1i*x);
                otherwise
                    error('domain unknown')
            end
        end
    end
    methods (Static)
        function domain=checkDomain(domain)
            domain=validatestring(domain,{'PLANE','DISC'});
        end
        function extrapolation=checkExtrapolation(extrapolation)
            extrapolation=validatestring(extrapolation,{'ZERO','INTERPOLATION'});
        end
        function checkParamData(x,Z)
            % check the parameter and data separately
            Interpolation.checkParam(x);
            Interpolation.checkData(Z);
            % verify that the length of both is equal
            if size(x,3)~=size(Z,3)
                error('The third dimension of the parameter and the data should match');
            end
        end
        function checkParam(x)
            validateattributes(x,{'numeric'},{'3d','size',[1 1 nan]});
        end
        function checkData(Z)
            validateattributes(Z,{'numeric'},{'3d'});
        end
    end
end

