close all
clear variables
clc

% get the data
datasource = 'RSS';
switch datasource
    case 'RSS'
        % generate a random state space example
        % N is the amount of stable poles in the random system
        N = 50;
        % I generate a random system without a perfect integrator
        p={1};
        while any(db(p{1}-1)<-200)
            % generate a random system of very high order
            sys = drss(N,1,1);
            % check whether there's a perfect integrator present in the system,
            % if there is, just retry
            [z,p,k] = zpkdata(sys);
        end
        % create the Z function
        Zfunc = @(x) freqresp(sys,x(:));
        % create the original frequency axis
        theta = reshape(linspace(0,pi,500),1,1,[]);
        Z = Zfunc(theta);
    otherwise
        error('Datasource not known')
end

plot(squeeze(theta),squeeze(db(Z)))

% profile on
tic;
% estimate the models
M = RationalInterpolation(theta,Z,'Domain','disc');
% set the new frequency axis
thetai = reshape(linspace(min(theta),max(theta),9001),1,1,[]);
% evaluate the models
Zrational = evaluate(M,thetai);
disp(['rational interpolation time = ' num2str(toc)]);
% profile off
% profile viewer

% determine the interpolation error
Zcorrect = Zfunc(thetai);
Erational = Zrational  - Zcorrect;

% plot the result
[M,N,~]=size(Z);
figure(3154321)
clf
ind=0;
for mm=1:M
    for nn=1:N
        ind =ind+1;
        subplot(N,M,ind)
        hold on
        plot(squeeze(theta) ,db(squeeze(Z(mm,nn,:)))      ,'+')
        plot(squeeze(thetai),db(squeeze(Zrational(mm,nn,:)))  ,'-')
        plot(squeeze(thetai),db(squeeze(Zcorrect(mm,nn,:)-Zrational(mm,nn,:)))  ,'x','MarkerSize',2);
        title(sprintf('Z(%d,%d)',mm,nn))
        xlabel('Frequency [Hz]')
        ylabel('Magnitude [dB]')
        legend('data','rational','error','Location','SW')
    end
end

% also plot the phase
figure(3216542)
ind=0;
for mm=1:M
    for nn=1:N
        ind =ind+1;
        subplot(N,M,ind)
        hold on
        plot(squeeze(theta ),angle(squeeze(Z(mm,nn,:)))      ,'+')
        plot(squeeze(thetai),angle(squeeze(Zrational(mm,nn,:)))  ,'-')
        title(sprintf('Z(%d,%d)',mm,nn))
        xlabel('Frequency [Hz]')
        ylabel('Phase [rad]')
        legend('data','rational','Location','SW')
    end
end




