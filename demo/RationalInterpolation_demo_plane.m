close all
clear variables
clc

% get the data
datasource = 'RSS';
switch datasource
    case 'SINGLE'
        % create a trivial test with a single complex pole
        Zfunc = @(x) polyval([1 1],1i*x)./polyval([1 -5*1i-1],1i*x);
        freq = reshape(linspace(0,10,100),1,1,[]);
        Z = Zfunc(freq);
    case 'MULTIPLE'
        % create a test with a few complex poles
        p = [-0.2 2*1i-0.1 -2*1i-0.1 5*1i+0.01 -5*1i+0.01];
        z = [4*1i-0.1 -4*1i-0.1];
        sys = zpk(z,p,10);
        % create the Z function
        Zfunc = @(x) freqresp(sys,x,'Hz');
        % create the original frequency axis
        freq = reshape(linspace(0,3,200),1,1,[]);
        Z = Zfunc(freq);
    case 'RSS'
        % generate a random state space example
        % N is the amount of stable poles in the random system
        N = 50;
        % I generate a random system without a perfect integrator
        p={0};
        while any(db(p{1})<-200)
            % generate a random system of very high order
            sys = rss(N,2,3);
            % check whether there's a perfect integrator present in the system,
            % if there is, just retry
            [z,p,k] = zpkdata(sys);
        end
        % add some delay to the input and the output, to maks stuff more difficult
        sys.OutputDelay = 0;
        sys.InputDelay = 0;
        % create the Z function
        Zfunc = @(x) freqresp(sys,x(:),'Hz');
        % create the original frequency axis
        freq = reshape(logspace(-1,0.5,5000),1,1,[]);
        Z = Zfunc(freq);
    otherwise
        error('Datasource not known')
end

profile on
tic;
% estimate the models
M = RationalInterpolation(freq,Z,'Domain','PLANE');
% set the new frequency axis
freqi = reshape(linspace(min(freq),max(freq),2001),1,1,[]);
% evaluate the models
Zrational = evaluate(M,freqi);
disp(['Rational interpolation time = ' num2str(toc)]);
profile off
profile viewer

% determine the interpolation error
Zcorrect = Zfunc(freqi);
Erational = Zrational  - Zcorrect;

% plot the result
[M,N,~]=size(Z);
figure(3154321)
clf
ind=0;
for mm=1:M
    for nn=1:N
        ind =ind+1;
        subplot(N,M,ind)
        hold on
        plot(squeeze(freq) ,db(squeeze(Z(mm,nn,:)))      ,'+')
        plot(squeeze(freqi),db(squeeze(Zrational(mm,nn,:)))  ,'-')
        plot(squeeze(freqi),db(squeeze(Zcorrect(mm,nn,:)-Zrational(mm,nn,:)))  ,'x','MarkerSize',2);
        title(sprintf('Z(%d,%d)',mm,nn))
        xlabel('Frequency [Hz]')
        ylabel('Magnitude [dB]')
        legend('data','rational','error','Location','SW')
    end
end

% also plot the phase
figure(3216542)
ind=0;
for mm=1:M
    for nn=1:N
        ind =ind+1;
        subplot(N,M,ind)
        hold on
        plot(squeeze(freq ),angle(squeeze(Z(mm,nn,:)))      ,'+')
        plot(squeeze(freqi),angle(squeeze(Zrational(mm,nn,:)))  ,'-')
        title(sprintf('Z(%d,%d)',mm,nn))
        xlabel('Frequency [Hz]')
        ylabel('Phase [rad]')
        legend('data','rational','Location','SW')
    end
end
