function Z_dc = padePredictDC(Z,freq)
% padePredictDC uses a local rational model of the two first points to
% predict the DC value of a frequency response function
%
%    dcval = padePredictDC(y,freq)
%

% make the frequency axis a column vector
freq = freq(:);
F = length(freq);
% check for negative frequencies already
if any(freq<0)
    error('the data should not contain negative frequencies already');
end

% check the size of Z
[M,N,F2]=size(Z);
if F2~=F
    error('the length of the frequency axis is not correct');
end

% if the response at zero is already in the list, just return that
DC = freq==0;
if any(DC)
    Z_dc = Z(:,:,DC);
    return;
end

% the frequency axis contains the first two frequencies and the negative
% copy of the first two frequencies
f = [-freq(2:-1:1);freq(1:2)];
% pre-allocate the result
Z_dc = zeros(M,N,1);
for mm=1:M
    for nn=1:N
        % construct the local data
        y = squeeze(Z(mm,nn,1:2));
        yl = [conj(y(2:-1:1));y];
        % estimate the local model
        A = [f.^2 f ones(4,1) -yl.*f -yl];
        [~,~,V] = svd(A);
        t = V(:,end);
        % evaluate the obtained model in DC
        Z_dc(mm,nn) = real(t(3)./t(5));
    end
end

end

