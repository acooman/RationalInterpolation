clear variables
close all
clc

%% generate a random ration linear function with M inputs and N outputs
M = 20;
N = 1;
num = rand(M,N,2)-0.5 + 1i*(rand(M,N,2)-0.5);

%% generate the x-values on which this model will be evaluated and interpolated
xmin = -0.5;
xmax = 0.5;
x  = reshape(linspace(xmin,xmax,100),1,1,[]);
xi = reshape(linspace(xmin,xmax,501),1,1,[]);
domain = 'DISC';
switch domain
    case 'PLANE'
        s  = 2*pi*1i*x;
        si = 2*pi*1i*xi;
    case 'DISC'
        s  = exp(1i*x);
        si = exp(1i*xi);
    otherwise
        error('domain unknown')
end

%% evaluate the model in the points
Z =  Zfun(s,num);
Zicorrect = Zfun(si,num);

%% perform the interpolation

M = LinearInterpolation(x,Z,'Domain',domain);
% evaluate the models
Zi = evaluate(M,xi);


% compute the interpolation error
err = Zi-Zicorrect;
if any(err(:)>1e-12)
    error('interpolation error too large')
end

if (N==1)||(M==1)
    figure(76513)
    clf
    subplot(121)
    hold on
    plot(squeeze(x),real(squeeze(Z)),'+');
    plot(squeeze(xi),real(squeeze(Zi)));
    plot(squeeze(xi),real(squeeze(err)));
    subplot(122)
    hold on
    plot(squeeze(x),imag(squeeze(Z)),'+');
    plot(squeeze(xi),imag(squeeze(Zi)));
    plot(squeeze(xi),imag(squeeze(err)));
end


%% Function to evaluate the model
function Z = Zfun(s,num)
Z = zeros(size(s));
for mm=1:size(num,1)
    for nn=1:size(num,2)
        Z(mm,nn,:)=polyval(squeeze(num(mm,nn,:)),s);
    end
end
end