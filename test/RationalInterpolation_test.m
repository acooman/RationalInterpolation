clear variables
close all
clc

%% generate a random ration function of order 2/1 with M inputs and N outputs
M = 20;
N = 1;
num = zeros(M,N,3);
for mm=1:M
    for nn=1:N
        num(mm,nn,:) = poly(rand(2,1)+1i*rand(2,1));
    end
end
den = poly(rand(1,1)+1i*rand(1,1));

%% generate the x-values on which this model will be evaluated and interpolated
xmin = -0.2;
xmax = 0.2;
x  = reshape(linspace(xmin,xmax,100),1,1,[]);
xi = reshape(linspace(xmin,xmax,501),1,1,[]);
domain = 'PLANE';
switch domain
    case 'PLANE'
        s  = 2*pi*1i*x;
        si = 2*pi*1i*xi;
    case 'DISC'
        s  = exp(1i*x);
        si = exp(1i*xi);
    otherwise
        error('domain unknown')
end

%% evaluate the model in the points
 Z =  Zfun(s,num,den);
dZ = dZfun(s,num,den);
Zicorrect = Zfun(si,num,den);

M = RationalInterpolation(x,Z,'Domain',domain,'FirstDerivative',dZ(:,:,1));

if (N==1)||(M==1)
    figure(65421)
    clf
    hold on
    plot(squeeze(x(1:end  )),db(squeeze(dZ )),'+');
    plot(squeeze(x(1:end-1)),db(squeeze(M.dZ)),'-');
    plot(squeeze(x(1:end-1)),db(squeeze(dZ(:,:,1:end-1)-M.dZ)),'k.');
end

%% perform the interpolation with known derivative to test whether it works
% estimate the models
M = RationalInterpolation(x,Z,'Domain',domain,'Derivative',dZ);
% evaluate the models
Zi = evaluate(M,xi);

if (N==1)||(M==1)
    figure(76513)
    clf
    hold on
    plot(squeeze(x),db(squeeze(Z)),'+');
    plot(squeeze(xi),db(squeeze(Zi)));
    plot(squeeze(xi),db(squeeze(Zi-Zicorrect)),'k');
end

% %% the interpolation function should work with a minimum of three points
% inds = [1 round(length(x)/2) length(x)];
% [dZi,N,M] = RationalInterpolation_EstimateDerivative(x(inds),Z(:,:,inds),domain);

%% Function to evaluate the model
function Z = Zfun(s,num,den)
    Z = zeros(size(s));
    for mm=1:size(num,1)
        for nn=1:size(num,2)
            Z(mm,nn,:)=polyval(squeeze(num(mm,nn,:)),s);
        end
    end
    Z = Z./polyval(den,s);
end
%% Function to evaluate the derivative of the model
function dZ = dZfun(s,num,den)
    dZ = zeros(size(s));
    for mm=1:size(num,1)
        for nn=1:size(num,2)
            dZ(mm,nn,:)=polyval(polyder(squeeze(num(mm,nn,:))),s);
        end
    end
    dZ = (dZ-den(1)*Zfun(s,num,den))./polyval(den,s);
end