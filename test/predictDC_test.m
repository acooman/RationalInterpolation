clear variables
close all
clc

% generate a random state space example
% N is the amount of stable poles in the random system
N = 10;
% I generate a random system without a perfect integrator
p={0};
while any(db(p{1})<-200)
    % generate a random system of very high order
    sys = rss(N,2,3);
    % check whether there's a perfect integrator present in the system,
    % if there is, just retry
    [z,p,k] = zpkdata(sys);
end
% create the frequency axis
freq = linspace(0.01,3,200);
Z = freqresp(sys,freq,'Hz');

% calculate the exact DC response
Z_dc_correct = freqresp(sys,0,'Hz');

% estimate the DC response
Z_dc_estim = padePredictDC(Z,freq);

% calculate the differenct
relErr = sum(sum(abs(Z_dc_estim-Z_dc_correct)./(abs(Z_dc_correct))));

disp(['The error is ' num2str(100*relErr) '%']);

if relErr>0.1
    Z_dc_estim
    Z_dc_correct
    figure(213545)
    clf
    hold on
    [M,N,F]=size(Z);
    for mm=1:M
        for nn=1:N
            Znm = [squeeze(conj(Z(mm,nn,3:-1:1)));Z_dc_correct(mm,nn);squeeze((Z(mm,nn,1:3)))];
            plot(real(Znm),imag(Znm),'+-');
            plot(real(Z_dc_estim(mm,nn)),imag(Z_dc_estim(mm,nn)),'ro');
        end
    end
end

