clear variables
close all
clc


handle = @(z) 1./z;

theta = reshape(linspace(0,2*pi,1001),1,1,[]);
z = exp(1i*theta);

I = FunctionHanldeInterpolation(handle,[],[],'Domain','DISC');

Gtest = evaluate(I,theta);

err = Gtest - handle(z);