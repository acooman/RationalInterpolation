classdef RationalInterpolation < Interpolation
    %RATIONALINTERPOLATION
    
    properties (Access=protected)
        n2 double
        n1 double
        d0 double
        FirstDerivative
    end
    
    properties
        dZ double
    end
    
    methods
        %% Constructor
        function obj = RationalInterpolation(varargin)
            p=inputParser();
            p.KeepUnmatched=true;
            p.addOptional('x',[],@Interpolation.checkParam);
            p.addOptional('Z',[],@Interpolation.checkData);
            p.addParameter('FirstDerivative',[],@isnumeric);
            p.addParameter('Derivative',[],@isnumeric);
            p.parse(varargin{:});
            args = p.Results;
            % call the Interpolation constructor without estimating
            obj = obj@Interpolation(p.Unmatched);
            % assign the user-provided derivatives
            obj.FirstDerivative = args.FirstDerivative;
            obj.dZ = args.Derivative;
            % when x and Z are provided, estimate the rational models
            if ~isempty(args.Z)
                obj = estimate(obj,args.x,args.Z);
            end
        end
    end
    
    methods (Access=protected)
        %% Implement the estimate function
        function obj = estimate_normalised(obj)
            % normalise the third point
            s2 = (obj.S(:,:,3:end)-obj.OffsetS(:,:,1:end-1))./obj.DeltaS(:,:,1:end-1);
            Z2 = (obj.Z(:,:,3:end)-obj.OffsetZ(:,:,1:end-1))./obj.DeltaZ(:,:,1:end-1);
            if isempty(obj.dZ)
                % estimate the derivative
                [obj,dZn] = estimate_derivative(obj,s2,Z2);
            else
                % normalise the provided derivative
                dZn = obj.dZ(:,:,1:end-1).*obj.DeltaS./obj.DeltaZ;
            end
            % preallocate the parameters
            obj.d0 = zeros(size(obj.Z,1),size(obj.Z,2),size(obj.Z,3)-1);
            obj.n1 = zeros(size(obj.Z,1),size(obj.Z,2),size(obj.Z,3)-1);
            obj.n2 = zeros(size(obj.Z,1),size(obj.Z,2),size(obj.Z,3)-1);
            % calculate the coefficients of the local models
            den = (dZn(:,:,1:end-1)-1).*s2.*s2 - dZn(:,:,1:end-1).*s2 + Z2;
            obj.d0(:,:,1:end-1) =  -s2.*( Z2 - s2 )./den;
            obj.n1(:,:,1:end-1) =  -dZn(:,:,1:end-1).*s2.*( Z2 - s2 )./den;
            obj.n2(:,:,1:end-1) = ( Z2 - s2.*(Z2 + dZn(:,:,1:end-1) - Z2.*dZn(:,:,1:end-1)) )./den;
            % estimate the coefficients of the last model
            obj.d0(:,:,end)=1./(dZn(:,:,end)-1);
            obj.n1(:,:,end)=dZn(:,:,end)./(dZn(:,:,end)-1);
            obj.n2(:,:,end)=0;
            if any(isnan(obj.d0(:)))||any(isnan(obj.n1(:)))||any(isnan(obj.n2(:)))
                error('Some of the estimated parameters are nan');
            end
        end
        %% Implement the evaluate function
        function Z = evaluate_normalised(obj,s,index)
            Z = (obj.n2(:,:,index).*s.^2 + obj.n1(:,:,index).*s)./(s+obj.d0(:,:,index));
        end
        %% function to estimate the normalised derivative
        function [obj,dZn] = estimate_derivative(obj,s2,Z2)
            dZn = zeros(size(obj.Z,1),size(obj.Z,2),size(obj.S,3)-1);
            % estimate the first derivative
            if isempty(obj.FirstDerivative)
                dZn(:,:,1) = Z2(:,:,1).*(s2(:,:,1)-1)./(s2(:,:,1).*(Z2(:,:,1)-1));
            else
                dZn(:,:,1) = obj.DeltaS(:,:,1)./obj.DeltaZ(:,:,1).*obj.FirstDerivative;
            end
            % estimate the other derivatives
            p1 = s2.*(Z2-1);
            p2 = s2.*s2-2.*s2.*Z2+Z2;
            p3 = s2.*(s2-1);
            p4 = Z2.*(1-s2);
            % loop over the data calculating the normalised derivatives
            DeltaS = obj.DeltaS;
            DeltaZ = obj.DeltaZ;
            for ff=2:size(dZn,3)
                % TODO: the normalisation/renormalisation can be thrown out of the for-loop to speed things up even more
                dZn(:,:,ff)=  (DeltaS(:,:,ff)./DeltaZ(:,:,ff)) .* (DeltaZ(:,:,ff-1)./DeltaS(:,:,ff-1)) ...
                    .*( p1(:,:,ff-1).*dZn(:,:,ff-1) + p2(:,:,ff-1) )./( p3(:,:,ff-1).*dZn(:,:,ff-1) + p4(:,:,ff-1) );
            end
            % denormalise the derivative
            obj.dZ = dZn.*obj.DeltaZ./obj.DeltaS;
        end
    end
    
    
end

