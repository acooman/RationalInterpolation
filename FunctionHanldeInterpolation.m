classdef FunctionHanldeInterpolation < Interpolation
    %FunctionHanldeInterpolation 
    
    properties
        FunctionHandle@function_handle
    end
    
    methods
        %% CONSTRUCTOR
        function obj = FunctionHanldeInterpolation(FunctionHandle,varargin)
            % call the superclass constructor
            obj = obj@Interpolation(varargin{:});
            % assign the function handle
            obj.FunctionHandle = FunctionHandle;
        end
    end
    
    methods 
        function Z = evaluate(obj,x)
            s=domainParamtrisation(obj,x);
            Z = obj.FunctionHandle(s);
        end
    end
    %% Implement the abstract methods
    methods (Access=protected)
        function Z = evaluate_normalised(obj,x,index)
            % we do nothing here, because we overwrite evaluate
            Z=0;
        end
        function obj = estimate_normalised(obj)
            % we have nothing to do here
        end
    end
end

